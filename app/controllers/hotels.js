import Ember from 'ember';

export default Ember.ObjectController.extend({
  needs: ['hotels/index'],
  name: Ember.computed.alias('controllers.hotels/index.name'),

  bufferedName: null,
  changeName: function() {
    this.set('name', this.get('bufferedName'));
  },
  bufferedNameDidChange: function() {
    Ember.run.debounce(this, this.changeName, 1000)
  }.observes('bufferedName')

});

