import Ember from 'ember';

export default Ember.ObjectController.extend({
  queryParams: ['name', 'price'],
  price: 1,
  name: null,
  time: function(){
    return new Date().toString()
  }.property()

});

