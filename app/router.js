import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('hotels', {path: '/'}, function(){
    this.route('index', {path: '/:city'});
  });
});

export default Router;
