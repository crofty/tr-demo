import Ember from 'ember';

export default Ember.Route.extend({

  queryParams: {
    name: {
      refreshModel: true
    }
  },
  model: function(params) {
    console.log(params)
    var array = Ember.A(['ace', 'hilton']).filter(function(x){
      if(params.name === undefined){
        return true
      }
      return x.match(new RegExp(params.name))
    });
    var model =  Ember.Object.create({hotels: array});

    var promise = new Ember.RSVP.Promise(function(resolve, reject){
      setTimeout(function(){
        resolve(model);
      }, 1000);
    });

    return promise;
  }
});
